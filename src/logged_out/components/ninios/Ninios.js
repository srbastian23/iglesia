import React, { Fragment, useEffect } from "react"
import PropTypes from "prop-types"
import HeadSection from "../ninios/HeadSection"
import SecondSection from "../ninios/SecondSection"

function Ninios(props) {
  const { selectNinios } = props;
  useEffect(() => {
    selectNinios();
  }, [selectNinios]);
    return (
      <Fragment>
        <HeadSection />
        <SecondSection />
      </Fragment>
    );
  }
  
  Ninios.propTypes = {
    selectNiños: PropTypes.func.isRequired
};
  
export default Ninios;